<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'userRegister.email' => ['maxmpvl@gmail.com'],

    'datePattern' => 'Y/m/d H:i',
];
