<?php

namespace common\models;

use Yii;
use \yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_news".
 *
 * @property integer $news_id
 * @property integer $news_created
 * @property integer $news_update
 * @property integer $news_status
 * @property string $news_title
 * @property string $news_anatation
 * @property string $news_text
 */
class News extends \yii\db\ActiveRecord
{
    const STATUS_DISABLE = 0;
    const STATUS_ENABLED = 1;

    public function statusLabel()
    {
        $labels = $this->statusLabels();
        return isset($labels[$this->news_status]) ? $labels[$this->news_status] : null;
    }

    public function statusLabels()
    {
        return [
            self::STATUS_DISABLE => 'Выключена',
            self::STATUS_ENABLED => 'Включена',
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['news_created', 'news_update'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['news_update'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_title', 'news_text', 'news_anatation'], 'required'],
            [['news_text'], 'string'],
            [['news_anatation', 'news_title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'news_id' => 'News ID',
            'news_created' => 'News Created',
            'news_update' => 'News Update',
            'news_status' => 'News Status',
            'news_title' => 'News Title',
            'news_anatation' => 'News Anatation',
            'news_text' => 'News Text',
        ];
    }
}
