<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Review;

/**
 * ReviewSearch represents the model behind the search form about `common\models\Review`.
 */
class ReviewSearch extends Review
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['review_id', 'review_user_id', 'review_object_id', 'review_created', 'review_status', 'review_value'], 'integer'],
            [['review_object', 'review_text'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Review::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'review_id' => $this->review_id,
            'review_user_id' => $this->review_user_id,
            'review_object_id' => $this->review_object_id,
            'review_created' => $this->review_created,
            'review_status' => $this->review_status,
            'review_value' => $this->review_value,
        ]);

        $query->andFilterWhere(['like', 'review_object', $this->review_object])
            ->andFilterWhere(['like', 'review_text', $this->review_text]);

        return $dataProvider;
    }
}
