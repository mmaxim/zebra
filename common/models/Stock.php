<?php

namespace common\models;

use Yii;
use \yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_akcii".
 *
 * @property integer $akcii_id
 * @property integer $akcii_created
 * @property integer $akcii_status
 * @property string $akcii_icon_bonus
 * @property integer $akcii_type
 * @property integer $akcii_skidka
 * @property integer $akcii_dop_skidka
 * @property integer $akcii_action_time
 * @property integer $akcii_start_time
 * @property integer $akcii_end_time
 * @property string $akcii_description
 */
class Stock extends \yii\db\ActiveRecord
{
    const STATUS_DISABLE = 0;
    const STATUS_ENABLED = 1;
    const STATUS_BID = 2;
    const STATUS_ACTIVE = 3;
    const STATUS_NOT_ACTIVE = 4;

    const TYPE_BASE = 0;
    const TYPE_BONUS = 1;

    public function statusLabel()
    {
        $labels = $this->statusLabels();
        return isset($labels[$this->akcii_status]) ? $labels[$this->akcii_status] : null;
    }

    public function statusLabels()
    {
        return [
            self::STATUS_DISABLE => 'Выключена',
            self::STATUS_ENABLED => 'Включена',
            self::STATUS_BID => 'Заявка',
            self::STATUS_ACTIVE => 'Активна',
            self::STATUS_NOT_ACTIVE => 'Не активна'
        ];
    }

    public function typeLabel()
    {
        $labels = $this->typeLabels();
        return isset($labels[$this->akcii_type]) ? $labels[$this->akcii_type] : null;
    }

    public function typeLabels()
    {
        return [
            self::TYPE_BASE => 'Обычная',
            self::TYPE_BONUS => 'Бонусная',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_akcii';
    }


    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['akcii_created'],
                ],
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['akcii_action_time', 'akcii_start_time', 'akcii_end_time', 'akcii_description'], 'required'],
            [['akcii_status', 'akcii_type', 'akcii_skidka', 'akcii_dop_skidka'], 'integer'],
            [['akcii_action_time', 'akcii_start_time', 'akcii_end_time'], 'date', 'format' => Yii::$app->params['datePattern']],
            [['akcii_description'], 'string'],
            [['akcii_icon_bonus'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'akcii_id' => 'Akcii ID',
            'akcii_created' => 'Akcii Created',
            'akcii_status' => 'Akcii Status',
            'akcii_icon_bonus' => 'Akcii Icon Bonus',
            'akcii_type' => 'Akcii Type',
            'akcii_skidka' => 'Akcii Skidka',
            'akcii_dop_skidka' => 'Akcii Dop Skidka',
            'akcii_action_time' => 'Akcii Action Time',
            'akcii_start_time' => 'Akcii Start Time',
            'akcii_end_time' => 'Akcii End Time',
            'akcii_description' => 'Akcii Description',
        ];
    }

    public function _convertStrToTime()
    {
        $this->akcii_action_time = strtotime($this->akcii_action_time);
        $this->akcii_start_time = strtotime($this->akcii_start_time);
        $this->akcii_end_time = strtotime($this->akcii_end_time);
    }
}
