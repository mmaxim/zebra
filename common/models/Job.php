<?php

namespace common\models;

use Yii;
use \yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_job".
 *
 * @property integer $job_id
 * @property integer $job_created
 * @property integer $job_status
 * @property string $job_icon_bonus
 * @property string $job_post
 * @property string $job_oklad
 * @property string $job_description
 * @property string $job_phone
 * @property string $job_email
 */
class Job extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_job';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['job_icon_bonus', 'job_post', 'job_oklad', 'job_description', 'job_phone', 'job_email'], 'required'],
            [['job_created', 'job_status'], 'integer'],
            [['job_icon_bonus', 'job_post', 'job_description', 'job_phone', 'job_email'], 'string', 'max' => 255],
            [['job_oklad'], 'string', 'max' => 24]
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['job_created'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'job_id' => 'Job ID',
            'job_created' => 'Job Created',
            'job_status' => 'Job Status',
            'job_icon_bonus' => 'Job Icon Bonus',
            'job_post' => 'Job Post',
            'job_oklad' => 'Job Oklad',
            'job_description' => 'Job Description',
            'job_phone' => 'Job Phone',
            'job_email' => 'Job Email',
        ];
    }
}
