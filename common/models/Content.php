<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_content".
 *
 * @property integer $content_id
 * @property integer $content_created
 * @property integer $content_status
 * @property string $content_title
 * @property string $content_text
 */
class Content extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content_created', 'content_title', 'content_text'], 'required'],
            [['content_created', 'content_status'], 'integer'],
            [['content_text'], 'string'],
            [['content_title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'content_id' => 'Content ID',
            'content_created' => 'Content Created',
            'content_status' => 'Content Status',
            'content_title' => 'Content Title',
            'content_text' => 'Content Text',
        ];
    }
}
