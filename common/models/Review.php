<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_review".
 *
 * @property integer $review_id
 * @property integer $review_user_id
 * @property integer $review_object_id
 * @property string $review_object
 * @property integer $review_created
 * @property integer $review_status
 * @property string $review_text
 * @property integer $review_value
 */
class Review extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_review';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['review_user_id', 'review_object_id', 'review_created', 'review_text'], 'required'],
            [['review_user_id', 'review_object_id', 'review_created', 'review_status', 'review_value'], 'integer'],
            [['review_text'], 'string'],
            [['review_object'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'review_id' => 'Review ID',
            'review_user_id' => 'Review User ID',
            'review_object_id' => 'Review Object ID',
            'review_object' => 'Review Object',
            'review_created' => 'Review Created',
            'review_status' => 'Review Status',
            'review_text' => 'Review Text',
            'review_value' => 'Review Value',
        ];
    }
}
