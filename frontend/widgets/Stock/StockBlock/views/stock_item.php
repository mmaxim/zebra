<?php
use yii\helpers\Html;
use common\models\Stock;

/* @var $modelStock Stock*/

?>

<a href="#" title="" class="item">
    <div class="flag_block">
        <span class="discount">-25%</span>
        <span class="bonus"></span>
    </div>
    <?=Html::img($modelStock->getImageStock(), ['alt'=>'pic']);?>
    <div class="item_text">
        <?=$modelStock->akcii_description;?>
    </div>
    <div class="hover_block">
        <div class="time"><i class="ico"></i> До конца акции:<br> <span>10</span> дней <span>12:30</span></div>
        <div class="like"><i class="ico"></i> 0/div>
    </div>
</a>