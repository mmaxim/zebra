<?php
use yii\helpers\Html;
use common\models\Stock;

/* @var $modelStock Stock*/

?>

                <div class="news_block">
                    <p class="bold">12.12.2014</p>
                    <p class="bold">Акция для постоянных клиентов!</p>
                    <div class="pic">
                        <img src="img/news_pic.png" alt="pic">
                    </div>
                    <p>Только в январе при размещении акции со скидкой более 50% вы можете получить такую же скидку...</p>
                    <a href="#" title="Подробнее...">Подробнее...</a>
                </div>