<?php
namespace frontend\widgets\Stock;

use yii\base\Widget;
use yii\helpers\VarDumper;
use \yii\helpers\ArrayHelper;

class StockItemWidget extends Widget
{
    public $modelStock;

    public function run()
    {
        return $this->render('stock_item',[
            'modelStock'=> $this->modelStock,
        ]);
    }
}