//	Datepicker ru
$(function($){
	$.datepicker.regional['ru'] = {
			closeText: '�������',
			prevText: '&#x3c;����',
			nextText: '����&#x3e;',
			currentText: '��������� � ������� ����',
			monthNames: ['������,','�������,','����,','������,','���,','����,',
			'����,','������,','��������,','�������,','������,','�������,'],
			monthNamesShort: ['���','���','���','���','���','���',
			'���','���','���','���','���','���'],
			dayNames: ['�����������','�����������','�������','�����','�������','�������','�������'],
			dayNamesShort: ['���','���','���','���','���','���','���'],
			dayNamesMin: ['��','��','��','��','��','��','��'],
			weekHeader: '��',
			dateFormat: 'dd.mm.yy',
			firstDay: 1,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ''};
		$.datepicker.setDefaults($.datepicker.regional['ru']);
});
$(document).ready(function(){
//	Cities dropdown
	$('.city').click(function(){
		$(this).toggleClass('active');
		$('.city_popup').slideToggle();		
	});
	$('.city_popup .close').click(function(){
		$('.city_popup').slideUp();
		$('.city').removeClass('active');
	});
	$('.city_popup input').keyup(function(){
		var in_val = $(this).val();
		if(in_val != "") {
			$('.city_popup .clear').css('display','block');
		} else {
			$('.city_popup .clear').hide();
		}
		$('.city_popup .clear').click(function(){
			$(this).prev('input').val('').focus();
			$('.city_popup .clear').hide();
		});
	});
//	Help dropdown
	$('.help').click(function(){
		$(this).toggleClass('active');
		$('.help_dropdown').slideToggle();
	});
//	Header slider
	$('.header_slider .bxslider').bxSlider({
		auto: true,
		controls: false
	});
	$('.right_slider .bxslider').bxSlider({
		auto: true,
		pager: false
	});
//	Categories dropdown
	$('.cat_menu li').click(function(){
		$('.cat_menu li').removeClass('active');
		$('.cat_menu li .dropdown').slideUp();
		if ($(this).children('.dropdown').is(':hidden')) {
			$(this).toggleClass('active');
			$(this).children('.dropdown').slideToggle();
		}
	});
//	Site search
	$('.search input').keyup(function(){
		var in_val = $(this).val();
		if(in_val != "") {
			$('.search .clear').css('display','block');
		} else {
			$('.search .clear').hide();
		}
		$('.search .clear').click(function(){
			$(this).prev('input').val('').focus();
			$('.search .clear').hide();
		});
	});	
//	Datepicker	
	$(".calendar").datepicker({ showButtonPanel: true});
	$(".calendar .ui-datepicker").hide();
	$(".calendar > span").click(function(){
		$(this).toggleClass('active');
		$(this).next('.ui-datepicker').slideToggle();
	});
//	To up
	$(".to_up").click(function(){
		$("html, body").animate({scrollTop:0},"slow");
	 });
});