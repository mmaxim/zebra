<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\models\SignupForm */

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php if ($type):?>
                <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                <?php if ($type == 'private'):?>
                    <?= $form->field($model, 'user_private_firstname') ?>
                    <?= $form->field($model, 'user_private_lastname') ?>
                    <?= $form->field($model, 'user_private_data_birth') ?>
                <?php endif;?>
                <?php if ($type == 'firm'):?>
                    <?= $form->field($model, 'user_firm_name') ?>
                    <?= $form->field($model, 'user_firm_contact_name') ?>
                    <?= $form->field($model, 'user_firm_description') ?>
                <?php endif;?>
                <?= $form->field($model, 'user_email') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'password_repeat')->passwordInput() ?>
                <?= $form->field($model, 'user_invite_code') ?>
                <div class="form-group">
                    <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            <?php else:?>
                <?=Html::a('Пользователь', ['/user/user/signup', 'type'=>'private']);?>
                <?=Html::a('Фирма', ['/user/user/signup', 'type'=>'firm']);?>
            <?php endif;?>
        </div>
    </div>
</div>
