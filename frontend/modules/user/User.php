<?php

namespace frontend\modules\user;

use \yii\base\Module;

class User extends Module
{
    public $controllerNamespace = 'frontend\modules\user\controllers';

    public function init()
    {
        parent::init();
    }
}