<?php
namespace frontend\modules\user\models;

use frontend\modules\user\models\User;
use yii\base\Model;
use Yii;
use yii\helpers\VarDumper;

/**
 * Register form
 */
class RegisterForm extends Model
{
    public $user_email;
    public $password;
    public $password_repeat;
    public $user_invite_code;

    public $user_private_firstname;
    public $user_private_lastname;
    public $user_private_data_birth;
    public $user_city;
    public $user_firm_description;
    public $user_firm_contact_name;
    public $user_firm_name;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['user_email', 'filter', 'filter' => 'trim'],
            ['user_email', 'required'],
            ['user_email', 'email'],
            ['user_email', 'unique', 'targetClass' => 'frontend\modules\user\models\User', 'message' => 'This email address has already been taken.'],

            [['password', 'password_repeat'], 'required'],
            ['password', 'string', 'min' => 6],

            [['user_city', 'user_invite_code'], 'string'],

            ['password', 'compare', 'compareAttribute'=>'password_repeat'],

            [['user_private_firstname', 'user_private_lastname', 'user_private_data_birth'], 'required', 'on' => 'private'],
            [['user_firm_description', 'user_firm_contact_name', 'user_firm_name'], 'required', 'on' => 'firm'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            /* @var  $user User */
            $user = new User();
            foreach($this->attributes as $name=>$value){
                if (!in_array($name, ['password_repeat'])){
                    $user->$name= $value;
                }
            }
            $user->user_status= User::STATUS_NOT_ACTIVE;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->save();
            return $user;
        }

        return null;
    }
}
