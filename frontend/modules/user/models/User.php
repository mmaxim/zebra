<?php
namespace frontend\modules\user\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * User model
 *
 * @property integer $user_id
 * @property integer $user_created
 * @property string $user_email
 * @property string $user_password
 * @property integer $user_type
 * @property integer $user_status
 * @property string $user_avatar
 * @property string $user_private_firstname
 * @property string $user_private_lastname
 * @property string $user_private_data_birth
 * @property string $user_city
 * @property string $user_firm_name
 * @property string $user_firm_description
 * @property string $user_firm_contact_name
 * @property string $user_active_code
 * @property string $user_auth_key
 * @property string $user_invite_code
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    const TYPE_PRIVATE = 0;
    const TYPE_FIRM = 1;


    public $auth_key;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_user';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class'=>TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['user_created'],
                ],
            ]
        ];
    }

    /**
      * @inheritdoc
      */
    public function rules()
    {
        return [
            ['user_status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_NOT_ACTIVE]],
            ['user_type', 'in', 'range' => [self::TYPE_PRIVATE, self::TYPE_FIRM]],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['user_id' => $id, 'user_status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $email
     * @return static|null
     */
    public static function findByUsername($email)
    {
        return static::findOne(['user_email' => $email]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->user_auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->user_password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->user_password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->user_auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function MailConfirmRegistration()
    {
        //TODO: узнать за шаблон
        $body= "Для подтверждения регистрации перейлите по ". Html::a('ссылке', Yii::$app->urlManager->createAbsoluteUrl(['user/user/active', 'code'=>$this->user_auth_key]));
        $subject= "Регистрация на сайте";

        return Yii::$app->mailer->compose()
            ->setTo(\Yii::$app->params['userRegister.email'])
            ->setFrom([$this->user_email => $this->user_email])
            ->setSubject($subject)
            ->setTextBody($body)
            ->send();
    }
}
