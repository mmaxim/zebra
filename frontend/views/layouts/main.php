<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="main_wrapper">
    <div id="wrap">
    <div id="main" class="clearfix">
    <div class="header">
        <div class="top">
            <div class="wrapper">
                <div class="left">
                    <div class="city">
                        <span class="text">Город</span>
                        <i class="ico"></i>
                    </div>
                    <div class="city_popup">
                        <div class="wrapper">
                            <span title="Закрыть" class="close"></span>
                            <div class="input">
                                <input type="text" placeholder="Введите город">
                                <i title="Очистить" class="clear"></i>
                                <span>или выберите из списка:</span>
                            </div>
                            <div class="alphabet">
                                <a class="active" href="#" title="А">А</a>
                                <a href="#" title="Б">Б</a>
                                <a href="#" title="В">В</a>
                                <a href="#" title="Г">Г</a>
                                <a href="#" title="Д">Д</a>
                                <a href="#" title="Все города">Все города</a>
                            </div>
                            <div class="cities">
                                <div class="col">
                                    <a href="#" title="Город">Город</a>
                                    <a href="#" title="Город">Город</a>
                                    <a href="#" title="Город">Город</a>
                                    <a href="#" title="Город">Город</a>
                                    <a href="#" title="Город">Город</a>
                                </div>
                                <div class="col">
                                    <a href="#" title="Город">Город</a>
                                    <a href="#" title="Город">Город</a>
                                    <a href="#" title="Город">Город</a>
                                    <a href="#" title="Город">Город</a>
                                    <a href="#" title="Город">Город</a>
                                </div>
                                <div class="col">
                                    <a href="#" title="Город">Город</a>
                                    <a href="#" title="Город">Город</a>
                                    <a href="#" title="Город">Город</a>
                                    <a href="#" title="Город">Город</a>
                                    <a href="#" title="Город">Город</a>
                                </div>
                                <div class="col">
                                    <a href="#" title="Город">Город</a>
                                    <a href="#" title="Город">Город</a>
                                    <a href="#" title="Город">Город</a>
                                    <a href="#" title="Город">Город</a>
                                    <a href="#" title="Город">Город</a>
                                </div>
                                <div class="col">
                                    <a href="#" title="Город">Город</a>
                                    <a href="#" title="Город">Город</a>
                                    <a href="#" title="Город">Город</a>
                                    <a href="#" title="Город">Город</a>
                                    <a href="#" title="Город">Город</a>
                                </div>
                                <div class="cleaner"></div>
                                <a href="#" title=""></a>
                            </div>
                        </div>
                        <div class="line"></div>
                    </div>
                    <div class="help">
                        <span class="text">Помощь</span>
                        <i class="ico"></i>
                        <div class="help_dropdown">
                            <ul>
                                <li><a href="#" title="Правила сервиса">Правила сервиса</a></li>
                                <li><a href="#" title="Частые вопросы">Частые вопросы</a></li>
                                <li><a href="#" title="Связаться с нами">Связаться с нами</a></li>
                            </ul>
                            <div class="line"></div>
                        </div>
                    </div>
                    <div class="cleaner"></div>
                </div>
                <div class="right">
                    <a href="#" title="Войти">Войти</a>
                    <a href="#" title="Регистрация">Регистрация</a>
                    <!--								<a href="#" title="Выйти">Выйти</a>
                                                        <a href="#" title="Личный кабинет">Личный кабинет</a>-->
                </div>
                <div class="cleaner"></div>
            </div>
        </div>
        <div class="bottom">
            <div class="line"></div>
            <div class="wrapper">
                <a href="#" title="Зебра купон" class="logo"></a>
                <div class="header_slider">
                    <ul class="bxslider">
                        <li><img src="img/header_slider_pic.png" alt=""></li>
                        <li><img src="img/header_slider_pic.png" alt=""></li>
                        <li><img src="img/header_slider_pic.png" alt=""></li>
                    </ul>
                </div>
                <div class="cleaner"></div>
            </div>
            <div class="line"></div>
        </div>
    </div>
    <div class="container">
    <div class="categories">
        <div class="wrapper">
            <ul class="cat_menu">
                <li><a href="#" title="Все">Все</a></li>
                <li>Товары
                    <i class="ico"></i>
                    <div class="dropdown">
                        <ul>
                            <li><a href="#" title="Детские товары">Детские товары</a></li>
                            <li><a href="#" title="Книги, канцтовары">Книги, канцтовары</a></li>
                            <li><a href="#" title="Косметика">Косметика</a></li>
                            <li><a href="#" title="Мебель">Мебель</a></li>
                        </ul>
                    </div>
                </li>
                <li>Услуги
                    <i class="ico"></i>
                    <div class="dropdown">
                        <ul>
                            <li><a href="#" title="Авто">Авто</a></li>
                            <li><a href="#" title="Банки">Банки</a></li>
                            <li><a href="#" title="Бытовые">Бытовые</a></li>
                            <li><a href="#" title="Еда">Еда</a></li>
                        </ul>
                    </div>
                </li>
                <li>Здоровье
                    <i class="ico"></i>
                    <div class="dropdown">
                        <ul>
                            <li><a href="#" title="Аптеки">Аптеки</a></li>
                            <li><a href="#" title="Массаж">Массаж</a></li>
                            <li><a href="#" title="Медцентры">Медцентры</a></li>
                            <li><a href="#" title="Оптика">Оптика</a></li>
                        </ul>
                    </div>
                </li>
                <li>Обучение
                    <i class="ico"></i>
                    <div class="dropdown">
                        <ul>
                            <li><a href="#" title="Автошколы">Автошколы</a></li>
                            <li><a href="#" title="Для детей">Для детей</a></li>
                            <li><a href="#" title="Курсы">Курсы</a></li>
                            <li><a href="#" title="Репититорство">Репититорство</a></li>
                        </ul>
                    </div>
                </li>
                <li>Развлечения
                    <i class="ico"></i>
                    <div class="dropdown">
                        <ul>
                            <li><a href="#" title="Гостиницы">Гостиницы</a></li>
                            <li><a href="#" title="Клубы">Клубы</a></li>
                            <li><a href="#" title="Путешествия">Путешествия</a></li>
                            <li><a href="#" title="Другое">Другое</a></li>
                        </ul>
                    </div>
                </li>
                <li>Свадьба
                    <i class="ico"></i>
                    <div class="dropdown">
                        <ul>
                            <li><a href="#" title="Авто">Авто</a></li>
                            <li><a href="#" title="Ведущие">Ведущие</a></li>
                            <li><a href="#" title="Рестораны">Рестораны</a></li>
                            <li><a href="#" title="Другое">Другое</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
            <div class="search">
                <form>
                    <input type="text" placeholder="Поиск">
                    <i class="clear" title="Очистить"></i>
                </form>
            </div>
            <div class="cleaner"></div>
            <div class="calend_directory">
                <div class="calendar go_back"><span><i class="ico"></i> Календарь <i class="arrow"></i></span></div>
                <a href="#" title="" class="directory"><i class="ico"></i> <span>Справочник</span></a>
            </div>
            <div class="cleaner"></div>
        </div>
        <div class="line"></div>
    </div>
    <div class="wrapper">
        <div class="center">
            <div class="items_wrapp">
                <a href="#" title="" class="item">
                    <div class="flag_block">
                        <span class="discount">-25%</span>
                        <span class="bonus"></span>
                    </div>
                    <img src="img/item_pic.png" alt="pic">
                    <div class="item_text">
                        «Sushiset»: суши за полцены и клубная карта в подарок
                    </div>
                    <div class="hover_block">
                        <div class="time"><i class="ico"></i> До конца акции:<br> <span>10</span> дней <span>12:30</span></div>
                        <div class="like"><i class="ico"></i> 201</div>
                    </div>
                </a>
                <a href="#" title="" class="item">
                    <div class="flag_block">
                        <span class="discount">-25%</span>
                        <span class="bonus"></span>
                    </div>
                    <img src="img/item_pic.png" alt="pic">
                    <div class="item_text">
                        «Sushiset»: суши за полцены и клубная карта в подарок
                    </div>
                    <div class="hover_block">
                        <div class="time"><i class="ico"></i> До конца акции:<br> <span>10</span> дней <span>12:30</span></div>
                        <div class="like"><i class="ico"></i> 201</div>
                    </div>
                </a>
                <a href="#" title="" class="item">
                    <div class="flag_block">
                        <span class="discount">-25%</span>
                        <span class="bonus"></span>
                    </div>
                    <img src="img/item_pic.png" alt="pic">
                    <div class="item_text">
                        «Sushiset»: суши за полцены и клубная карта в подарок
                    </div>
                    <div class="hover_block">
                        <div class="time"><i class="ico"></i> До конца акции:<br> <span>10</span> дней <span>12:30</span></div>
                        <div class="like"><i class="ico"></i> 201</div>
                    </div>
                </a>
                <a href="#" title="" class="item">
                    <div class="flag_block">
                        <span class="discount">-25%</span>
                        <span class="bonus"></span>
                    </div>
                    <img src="img/item_pic.png" alt="pic">
                    <div class="item_text">
                        «Sushiset»: суши за полцены и клубная карта в подарок
                    </div>
                    <div class="hover_block">
                        <div class="time"><i class="ico"></i> До конца акции:<br> <span>10</span> дней <span>12:30</span></div>
                        <div class="like"><i class="ico"></i> 201</div>
                    </div>
                </a>
                <a href="#" title="" class="item">
                    <div class="flag_block">
                        <span class="discount">-25%</span>
                        <span class="bonus"></span>
                    </div>
                    <img src="img/item_pic.png" alt="pic">
                    <div class="item_text">
                        «Sushiset»: суши за полцены и клубная карта в подарок
                    </div>
                    <div class="hover_block">
                        <div class="time"><i class="ico"></i> До конца акции:<br> <span>10</span> дней <span>12:30</span></div>
                        <div class="like"><i class="ico"></i> 201</div>
                    </div>
                </a>
                <a href="#" title="" class="item">
                    <div class="flag_block">
                        <span class="discount">-25%</span>
                        <span class="bonus"></span>
                    </div>
                    <img src="img/item_pic.png" alt="pic">
                    <div class="item_text">
                        «Sushiset»: суши за полцены и клубная карта в подарок
                    </div>
                    <div class="hover_block">
                        <div class="time"><i class="ico"></i> До конца акции:<br> <span>10</span> дней <span>12:30</span></div>
                        <div class="like"><i class="ico"></i> 201</div>
                    </div>
                </a>
                <div class="cleaner"></div>
            </div>
            <a href="#" class="show_more" title="Показать еще">Показать еще</a>
        </div>
        <div class="right">
            <div class="bonus_block">
                <a href="#" title="" class="get_bonus">Заработай бонусы!</a>
                <!--								<p class="your_bonuses">Ваши бонусы: <span><i>00</i>20</span></p>
                                                    <p class="your_place">Место в <a href='#' title="Рейтинг">рейтинге</a>: 1098</p>-->
                <div class="line"></div>
            </div>
            <div class="right_slider">
                <a class="title" href="#" title="Печатное издание"><i class="ico"></i> <span>Печатное издание</span></a>
                <ul class="bxslider">
                    <li><a href="#"><img src="img/right_slider_pic.png" alt=""></a></li>
                    <li><a href="#"><img src="img/right_slider_pic.png" alt=""></a></li>
                    <li><a href="#"><img src="img/right_slider_pic.png" alt=""></a></li>
                </ul>
                <div class="line"></div>
            </div>
            <div class="job">
                <a class="title" href="#" title="Вакансии"><i class="ico"></i> <span>Вакансии</span></a>
                <div class="job_block">
                    <p class="bold">Водители категории C, D</p>
                    <p>Работодатель: ООО «Промсервис»</p>
                    <p>Оклад: от 30 000 руб.</p>
                    <a href="#" title="Подробнее...">Подробнее...</a>
                </div>
                <a class="view_all" href="#" title="Смотреть все вакансии">Смотреть все вакансии</a>
                <div class="line"></div>
            </div>
            <div class="news">
                <a class="title" href="#" title="Новости"><i class="ico"></i> <span>Новости</span></a>
                <div class="news_block">
                    <p class="bold">12.12.2014</p>
                    <p class="bold">Акция для постоянных клиентов!</p>
                    <div class="pic">
                        <img src="img/news_pic.png" alt="pic">
                    </div>
                    <p>Только в январе при размещении акции со скидкой более 50% вы можете получить такую же скидку...</p>
                    <a href="#" title="Подробнее...">Подробнее...</a>
                </div>
                <a class="view_all" href="#" title="Смотреть все новости">Смотреть все новости</a>
            </div>
        </div>
        <div class="cleaner"></div>
    </div>
    </div>
    </div>
    </div>
    <div id="footer">
        <div class="top">
            <div class="line"></div>
            <div class="wrapper">
                <a href="javascript:void(0);" class="to_up" title="Вверх"><span>Вверх</span></a>
                <div class="col">
                    <p>Компания</p>
                    <ul>
                        <li><a href="#" title="О «Зебре»">- О «Зебре»</a></li>
                        <li><a href="#" title="Новости">- Новости</a></li>
                        <li><a href="#" title="Бонусная программа">- Бонусная программа</a></li>
                    </ul>
                </div>
                <div class="col">
                    <p>Помощь</p>
                    <ul>
                        <li><a href="#" title="Правила сервиса">- Правила сервиса</a></li>
                        <li><a href="#" title="Частые вопросы">- Частые вопросы</a></li>
                        <li><a href="#" title="Связаться с нами">- Связаться с нами</a></li>
                    </ul>
                </div>
                <div class="col">
                    <p>Партнерам</p>
                    <ul>
                        <li><a href="#" title="Презентация сервиса">- Презентация сервиса</a></li>
                        <li><a href="#" title="Cотрудничество">- Cотрудничество</a></li>
                        <li><a href="#" title="Наши проекты">- Наши проекты</a></li>
                    </ul>
                </div>
                <div class="col">
                    <p>Мы в сети</p>
                    <ul>
                        <li><a href="#" title="ВКонтакте">- ВКонтакте</a></li>
                        <li><a href="#" title="Одноклассники">- Одноклассники</a></li>
                        <li><a href="#" title="Facebook">- Facebook</a></li>
                    </ul>
                </div>
                <div class="col">
                    <p>Контакты</p>
                    <ul>
                        <li>8(496) 416-14-61</li>
                        <li>8 964 537 08 08</li>
                        <li>ina4e@ina4e.ru</li>
                    </ul>
                </div>
                <div class="cleaner"></div>
            </div>
            <div class="line"></div>
        </div>
        <div class="bottom">
            <div class="wrapper">
                <div class="partners">
                    <span>Наши партнеры:</span>
                    <a href="#" title=""><img src="img/partner_pic.png" alt="pic"></a>
                    <a href="#" title="Радиохолдинг «Роалан»">Радиохолдинг <br> «Роалан»</a>
                </div>
                <div class="copyright">
                    <p>© 2014 «Зебра-Купон». <span>18+</span> Все права защищены.</p>
                </div>
                <div class="cleaner"></div>
            </div>
        </div>
    </div>
    </div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
