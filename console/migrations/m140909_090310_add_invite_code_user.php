<?php

use yii\db\Schema;
use yii\db\Migration;

class m140909_090310_add_invite_code_user extends Migration
{
    public function up()
    {
        $this->execute("
ALTER TABLE `tbl_user`
	ADD COLUMN `user_invite_code` VARCHAR(127) NULL DEFAULT '' AFTER `user_firm_contact_name`,
	CHANGE COLUMN `user_active_code` `user_active_code` VARCHAR(127) NOT NULL DEFAULT '' AFTER `user_invite_code`;
        ");
    }

    public function down()
    {
        $this->dropColumn('tbl_user', 'user_invite_code');
    }
}
