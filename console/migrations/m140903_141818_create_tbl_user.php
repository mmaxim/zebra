<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_141818_create_tbl_user extends Migration
{
    public function up()
    {
        $this->execute("
        CREATE TABLE `tbl_user` (
	`user_id` INT(11) NOT NULL AUTO_INCREMENT,
	`user_created` INT(11) NOT NULL,
	`user_email` VARCHAR(255) NOT NULL,
	`user_password` VARCHAR(255) NOT NULL,
	`user_type` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`user_status` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`user_avatar` VARCHAR(255) NULL DEFAULT '',
	`user_private_firstname` VARCHAR(255) NULL DEFAULT '',
	`user_private_lastname` VARCHAR(255) NULL DEFAULT '',
	`user_private_data_birth` VARCHAR(255) NULL DEFAULT '',
	`user_city` VARCHAR(255) NULL DEFAULT '',
	`user_firm_name` VARCHAR(255) NULL DEFAULT '',
	`user_firm_description` VARCHAR(512) NULL DEFAULT '',
	`user_firm_contact_name` VARCHAR(255) NULL DEFAULT '',
	`user_active_code` VARCHAR(127) NOT NULL,
	PRIMARY KEY (`user_id`),
	INDEX `user_status` (`user_status`),
	INDEX `user_type` (`user_type`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
        ");
    }

    public function down()
    {
        $this->dropTable('tbl_user');
    }
}
