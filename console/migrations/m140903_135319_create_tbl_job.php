<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_135319_create_tbl_job extends Migration
{
    public function up()
    {
        $this->execute("
CREATE TABLE `tbl_job` (
	`job_id` INT(11) NOT NULL AUTO_INCREMENT,
	`job_created` INT(11) NOT NULL,
	`job_status` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`job_icon_bonus` VARCHAR(255) NOT NULL,
	`job_post` VARCHAR(255) NOT NULL,
	`job_oklad` VARCHAR(24) NOT NULL,
	`job_description` VARCHAR(255) NOT NULL,
	`job_phone` VARCHAR(255) NOT NULL,
	`job_email` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`job_id`),
	INDEX `job_status` (`job_status`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

        ");
    }

    public function down()
    {
        $this->dropTable('tbl_job');
    }
}
