<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_135344_create_tbl_content extends Migration
{
    public function up()
    {
        $this->execute("
CREATE TABLE `tbl_content` (
	`content_id` INT(11) NOT NULL AUTO_INCREMENT,
	`content_created` INT(11) NOT NULL,
	`content_status` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`content_title` VARCHAR(255) NOT NULL,
	`content_text` TEXT NOT NULL,
	PRIMARY KEY (`content_id`),
	INDEX `content_status` (`content_status`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
        ");
    }

    public function down()
    {
        $this->dropTable('tbl_content');
    }
}
