<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_135328_create_tbl_review extends Migration
{
    public function up()
    {
        $this->execute("
        CREATE TABLE `tbl_review` (
	`review_id` INT(11) NOT NULL AUTO_INCREMENT,
	`review_user_id` INT(11) NOT NULL,
	`review_object_id` INT(11) NOT NULL,
	`review_object` VARCHAR(50) NOT NULL DEFAULT '',
	`review_created` INT(11) NOT NULL,
	`review_status` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`review_text` TEXT NOT NULL,
	`review_value` TINYINT(1) NOT NULL DEFAULT '0',
	PRIMARY KEY (`review_id`),
	INDEX `review_user_id` (`review_user_id`),
	INDEX `review_object_id` (`review_object_id`),
	INDEX `review_object` (`review_object`),
	INDEX `review_status` (`review_status`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
        ");
    }

    public function down()
    {
        $this->dropTable('tbl_review');
    }
}
