<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_135304_create_tbl_akcii extends Migration
{
    public function up()
    {
        $this->execute("
        CREATE TABLE `tbl_akcii` (
	`akcii_id` INT(11) NOT NULL AUTO_INCREMENT,
	`akcii_created` INT(11) NOT NULL,
	`akcii_status` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`akcii_icon_bonus` VARCHAR(255) NOT NULL,
	`akcii_type` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`akcii_skidka` SMALLINT(4) NOT NULL DEFAULT '0',
	`akcii_dop_skidka` SMALLINT(4) NOT NULL DEFAULT '0',
	`akcii_action_time` INT(11) NOT NULL,
	`akcii_start_time` INT(11) NOT NULL,
	`akcii_end_time` INT(11) NOT NULL,
	`akcii_description` TEXT NOT NULL,
	PRIMARY KEY (`akcii_id`),
	INDEX `akcii_status` (`akcii_status`),
	INDEX `akcii_type` (`akcii_type`),
	INDEX `akcii_start_time` (`akcii_start_time`),
	INDEX `akcii_end_time` (`akcii_end_time`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
        ");
    }

    public function down()
    {
        $this->dropTable('tbl_akcii');
    }
}
