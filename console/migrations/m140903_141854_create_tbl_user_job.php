<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_141854_create_tbl_user_job extends Migration
{
    public function up()
    {
        $this->execute("
        CREATE TABLE `tbl_user_job` (
	`user_id` INT(11) NOT NULL,
	`job_id` INT(11) NOT NULL,
	UNIQUE INDEX `user_id_job_id` (`user_id`, `job_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
        ");
    }

    public function down()
    {
        $this->dropTable('tbl_user_job');
    }
}
