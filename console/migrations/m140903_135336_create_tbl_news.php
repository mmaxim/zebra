<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_135336_create_tbl_news extends Migration
{
    public function up()
    {
        $this->execute("
        CREATE TABLE `tbl_news` (
	`news_id` INT(11) NOT NULL AUTO_INCREMENT,
	`news_created` INT(11) NOT NULL,
	`news_update` INT(11) NOT NULL,
	`news_status` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`news_title` VARCHAR(255) NOT NULL DEFAULT '',
	`news_anatation` VARCHAR(255) NOT NULL DEFAULT '',
	`news_text` TEXT NOT NULL,
	PRIMARY KEY (`news_id`),
	INDEX `news_status` (`news_status`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
        ");
    }

    public function down()
    {
        $this->dropTable('tbl_news');
    }
}
