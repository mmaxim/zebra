<?php

use yii\db\Schema;
use yii\db\Migration;

class m140909_100410_add_auth_user extends Migration
{
    public function up()
    {
        $this->execute("
 ALTER TABLE `tbl_user`
	ADD COLUMN `user_auth_key` VARCHAR(127) NULL DEFAULT '' AFTER `user_invite_code`;
        ");
    }

    public function down()
    {
        $this->dropColumn('tbl_user', 'user_auth_key');
    }
}
