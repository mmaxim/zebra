<?php

use yii\db\Schema;
use yii\db\Migration;

class m140903_141958_create_tbl_user_akcii extends Migration
{
    public function up()
    {
        $this->execute("
        CREATE TABLE `tbl_user_akcii` (
	`user_id` INT(11) NOT NULL,
	`akcii_id` INT(11) NOT NULL,
	UNIQUE INDEX `user_id_akcii_id` (`user_id`, `akcii_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
        ");
    }

    public function down()
    {
        $this->dropTable('tbl_user_akcii');
    }
}
