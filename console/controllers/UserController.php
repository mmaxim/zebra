<?php
/**
 * Created by PhpStorm.
 * User: Maksym
 * Date: 11.09.14
 * Time: 10:34
 */

namespace console\controllers;
use yii\console\Controller;
use backend\models\User;

class UserController extends Controller
{
    public function actionAdduser($user, $password)
    {
        /* @var $modelUser User*/
        $modelUser= new User();
        $modelUser->created_at= time();
        $modelUser->getAuthKey();
        $modelUser->email= $user.'@mail.ru';
        $modelUser->username= $user;
        $modelUser->password= $password;
        $modelUser->status= User::STATUS_ACTIVE;
        $modelUser->role= User::ROLE_USER;
        if ($modelUser->save()){
            echo "Ok!\n";
        }else{
            echo "Error!\n";
        }

    }
}