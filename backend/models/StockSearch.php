<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Stock;

/**
 * StockSearch represents the model behind the search form about `app\models\Stock`.
 */
class StockSearch extends Stock
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['akcii_id', 'akcii_created', 'akcii_status', 'akcii_type', 'akcii_skidka', 'akcii_dop_skidka', 'akcii_action_time', 'akcii_start_time', 'akcii_end_time'], 'integer'],
            [['akcii_icon_bonus', 'akcii_description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Stock::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'akcii_id' => $this->akcii_id,
            'akcii_created' => $this->akcii_created,
            'akcii_status' => $this->akcii_status,
            'akcii_type' => $this->akcii_type,
            'akcii_skidka' => $this->akcii_skidka,
            'akcii_dop_skidka' => $this->akcii_dop_skidka,
            'akcii_action_time' => $this->akcii_action_time,
            'akcii_start_time' => $this->akcii_start_time,
            'akcii_end_time' => $this->akcii_end_time,
        ]);

        $query->andFilterWhere(['like', 'akcii_icon_bonus', $this->akcii_icon_bonus])
            ->andFilterWhere(['like', 'akcii_description', $this->akcii_description]);

        return $dataProvider;
    }
}
