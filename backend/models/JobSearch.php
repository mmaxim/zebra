<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Job;

/**
 * JobSearch represents the model behind the search form about `common\models\Job`.
 */
class JobSearch extends Job
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['job_id', 'job_created', 'job_status'], 'integer'],
            [['job_icon_bonus', 'job_post', 'job_oklad', 'job_description', 'job_phone', 'job_email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Job::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'job_id' => $this->job_id,
            'job_created' => $this->job_created,
            'job_status' => $this->job_status,
        ]);

        $query->andFilterWhere(['like', 'job_icon_bonus', $this->job_icon_bonus])
            ->andFilterWhere(['like', 'job_post', $this->job_post])
            ->andFilterWhere(['like', 'job_oklad', $this->job_oklad])
            ->andFilterWhere(['like', 'job_description', $this->job_description])
            ->andFilterWhere(['like', 'job_phone', $this->job_phone])
            ->andFilterWhere(['like', 'job_email', $this->job_email]);

        return $dataProvider;
    }
}
