<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'sb-admin/css/bootstrap-datetimepicker.min.css',
        '//maxcdn.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css',
	    'sb-admin/css/sb-admin.css',
        'sb-admin/font-awesome/css/font-awesome.min.css',
        'http://cdn.oesmith.co.uk/morris-0.4.3.min.css',
        'plugins/summernote/summernote.css'
	];
    public $js = [
//        'sb-admin/js/bootstrap.js',
//        '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js',
        'plugins/summernote/summernote.js',
        '//maxcdn.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js',
        'sb-admin/js/bootstrap-datetimepicker.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',
//        'http://cdn.oesmith.co.uk/morris-0.4.3.min.js',
//        'sb-admin/js/morris/chart-data-morris.js',
        'sb-admin/js/tablesorter/jquery.tablesorter.js',
        'sb-admin/js/tablesorter/tables.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
