<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html">SB Admin</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Материалы <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="/stock/index">Акции</a></li>
                        <li><a href="/job/index">Вакансии</a></li>
                        <li><a href="/catalog/index">Справочники</a></li>
                        <li><a href="/news/index">Новости</a></li>
                        <li><a href="/content/index">Контент</a></li>
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right navbar-user">
                <li class="dropdown user-dropdown">
                    <?php if (Yii::$app->user->isGuest):?>
                        <li><?=Html::a('<i class="fa fa-user"></i> Login<b class="caret"></b>', 'site/login', ['class'=>'dropdown-toggle', 'data-toggle'=>'dropdown'])?></li>
                    <?php else:?>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?=Yii::$app->user->identity->username?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><?=Html::a('<i class="fa fa-power-off"></i> Log Out', 'site/logout')?></li>
<!--                            <li>--><?//=Html::a('<i class="fa fa-power-off"></i> Log Out', 'site/logout', ['data-method' => 'post'])?><!--</li>-->
                        </ul>
                    <?php endif;?>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>

    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            </div>
            <?= $content ?>
        </div>
    </div>


    <footer class="footer">
        <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
