<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Content */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs("
    $('#".Html::getInputId($model, 'content_text')."').summernote();
");

?>

<div class="container">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class='col-sm-2'>
            <div class="form-group">
                <?= $form->field($model, 'content_status')->textInput() ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class='col-sm-2'>
            <div class="form-group">
                <?= $form->field($model, 'content_title')->textInput(['maxlength' => 255]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class='col-sm-2'>
            <div class="form-group">
                <?= $form->field($model, 'content_text')->textarea(['rows' => 6]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

   <?php ActiveForm::end(); ?>

</div>
