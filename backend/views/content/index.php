<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Страницы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <div class="text-right">
            <?= Html::a('Create Content', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-money"></i> Список страниц</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'content_id',
                            [
                                'attribute' => 'content_created',
                                'format' => ['date', Yii::$app->params['datePattern']],
                            ],
                            [
                                'attribute' => 'content_status',
                                'value' => function ($model, $index, $widget) {
                                        return $model->statusLabel();
                                    },
                            ],
                            'content_title',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>