<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Job */

$this->title = 'Редактирование вакансии';
$this->params['breadcrumbs'][] = ['label' => 'Jobs', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="col-lg-10">
    <?php if (Yii::$app->session->hasFlash('success')):?>
    <div class="alert alert-success alert-dismissable" data-dismiss="alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?=Yii::$app->session->getFlash('success');?>
    </div>
    <?php endif;?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-6">
                    <h1><?= Html::encode($this->title) ?></h1>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
        </div>
    </div>
</div>
