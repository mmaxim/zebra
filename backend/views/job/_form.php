<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Job */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="container">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class='col-sm-2'>
            <div class="form-group">
                <?= $form->field($model, 'job_icon_bonus')->textInput(['maxlength' => 255]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class='col-sm-2'>
            <div class="form-group">
                <?= $form->field($model, 'job_post')->textInput(['maxlength' => 255]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class='col-sm-2'>
            <div class="form-group">
                <?= $form->field($model, 'job_oklad')->textInput(['maxlength' => 24]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class='col-sm-2'>
            <div class="form-group">
                <?= $form->field($model, 'job_description')->textInput(['maxlength' => 255]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class='col-sm-2'>
            <div class="form-group">
                <?= $form->field($model, 'job_phone')->textInput(['maxlength' => 255]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class='col-sm-2'>
            <div class="form-group">
                <?= $form->field($model, 'job_email')->textInput(['maxlength' => 255]) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
