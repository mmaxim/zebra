<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <div class="text-right">
            <?= Html::a('Create News', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-money"></i> Список акций</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'news_id',
                            [
                                'attribute' => 'news_created',
                                'format' => ['date', Yii::$app->params['datePattern']],
                            ],
                            [
                                'attribute' => 'news_update',
                                'format' => ['date', Yii::$app->params['datePattern']],
                            ],
                            [
                                'attribute' => 'news_status',
                                'value' => function ($model, $index, $widget) {
                                    return $model->statusLabel();
                                },
                            ],
                            'news_title',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>