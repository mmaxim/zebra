<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('/plugins/form-ckeditor/ckeditor.js');

$this->registerJs("
    $('#".Html::getInputId($model, 'news_anatation')."').summernote();
    $('#".Html::getInputId($model, 'news_text')."').summernote();
");
?>

<div class="container">
    <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class='col-sm-2'>
                <div class="form-group">
                    <?= $form->field($model, 'news_status')->dropDownList($model->statusLabels()) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class='col-sm-2'>
                <div class="form-group">
                    <?= $form->field($model, 'news_title')->textInput(['maxlength' => 255]) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class='col-sm-12'>
                <div class="form-group">
                    <?= $form->field($model, 'news_anatation')->textarea(['maxlength' => 255]) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class='col-sm-12'>
                <div class="form-group">
                    <?= $form->field($model, 'news_text')->textarea(['rows' => 6]) ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
