<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Stocks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <div class="text-right">
            <?= Html::a('Create Stock', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-money"></i> Список акций</h3>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'akcii_id',
                        [
                            'attribute' => 'akcii_created',
                            'format' => ['date', Yii::$app->params['datePattern']],
                        ],
                        [
                            'attribute' => 'akcii_status',
                            'value' => function ($model, $index, $widget) {
                                return $model->statusLabel();
                            },
                        ],
                        [
                            'attribute' => 'akcii_type',
                            'value' => function ($model, $index, $widget) {
                                return $model->typeLabel();
                            },
                        ],
                        [
                            'attribute' => 'akcii_action_time',
                            'format' => ['date', Yii::$app->params['datePattern']],
                        ],
                        [
                            'attribute' => 'akcii_start_time',
                            'format' => ['date', Yii::$app->params['datePattern']],
                        ],
                        [
                            'attribute' => 'akcii_end_time',
                            'format' => ['date', Yii::$app->params['datePattern']],
                        ],

                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
</div>
