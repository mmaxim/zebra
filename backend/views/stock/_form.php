<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Stock */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('/plugins/form-ckeditor/ckeditor.js');

$this->registerJs("
    CKEDITOR.config.allowedContent = true;
    CKEDITOR.editorConfig = function( config ) {
          config.extraAllowedContent = '*[id](*)';  // remove '[id]', if you don't want IDs for HTML tags
    }

    CKEDITOR.replace( '". Html::getInputId($model, 'akcii_description')."',
    {
        filebrowserUploadUrl : '/image/upload',
        filebrowserWindowWidth : '640',
        filebrowserWindowHeight : '480'
    });

    $('.form_datetime').datetimepicker();
");
?>

<div class="container">
    <div class="row">
        <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class='col-sm-2'>
                <div class="form-group">
                    <?= $form->field($model, 'akcii_status')->dropDownList($model->statusLabels());?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class='col-sm-2'>
                <div class="form-group">
                    <?= $form->field($model, 'akcii_type')->dropDownList($model->typeLabels());?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class='col-sm-1'>
                <div class="form-group">
                    <?= $form->field($model, 'akcii_skidka')->textInput() ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class='col-sm-1'>
                <div class="form-group">
                    <?= $form->field($model, 'akcii_dop_skidka')->textInput() ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class='col-sm-3'>
                <?= $form->field($model, 'akcii_action_time')->textInput(['class'=>'form_datetime form-control', 'value'=> date(Yii::$app->params['datePattern'], $model->akcii_action_time)]) ?>
            </div>
        </div>
        <div class="row">
            <div class='col-sm-3'>
                <?= $form->field($model, 'akcii_start_time')->textInput(['class'=>'form_datetime form-control', 'value'=> date(Yii::$app->params['datePattern'], $model->akcii_start_time)]) ?>
            </div>
        </div>
        <div class="row">
            <div class='col-sm-3'>
                <?= $form->field($model, 'akcii_end_time')->textInput(['class'=>'form_datetime form-control', 'value'=>date(Yii::$app->params['datePattern'], $model->akcii_end_time)]) ?>
            </div>
        </div>

        <?= $form->field($model, 'akcii_description')->textarea(['rows' => 10]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
