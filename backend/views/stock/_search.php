<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StockSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stock-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'akcii_id') ?>

    <?= $form->field($model, 'akcii_created') ?>

    <?= $form->field($model, 'akcii_status') ?>

    <?= $form->field($model, 'akcii_icon_bonus') ?>

    <?= $form->field($model, 'akcii_type') ?>

    <?php // echo $form->field($model, 'akcii_skidka') ?>

    <?php // echo $form->field($model, 'akcii_dop_skidka') ?>

    <?php // echo $form->field($model, 'akcii_action_time') ?>

    <?php // echo $form->field($model, 'akcii_start_time') ?>

    <?php // echo $form->field($model, 'akcii_end_time') ?>

    <?php // echo $form->field($model, 'akcii_description') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
