<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ReviewSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="review-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'review_id') ?>

    <?= $form->field($model, 'review_user_id') ?>

    <?= $form->field($model, 'review_object_id') ?>

    <?= $form->field($model, 'review_object') ?>

    <?= $form->field($model, 'review_created') ?>

    <?php // echo $form->field($model, 'review_status') ?>

    <?php // echo $form->field($model, 'review_text') ?>

    <?php // echo $form->field($model, 'review_value') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
